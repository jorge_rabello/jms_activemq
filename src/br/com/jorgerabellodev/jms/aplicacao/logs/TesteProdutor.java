package br.com.jorgerabellodev.jms.aplicacao.logs;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TesteProdutor {

    public static void main(String[] args) throws Exception {

        // estabelecendo uma conexão
        InitialContext context = new InitialContext();

        ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("ConnectionFactory");
        factory.setTrustAllPackages(true);

        Connection connection = factory.createConnection();
        connection.start();

        // cria um consumer
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination fila = (Destination) context.lookup("LOG");

        MessageProducer producer = session.createProducer(fila);

        // esta mensagem nao sera persistida
        // o nivel de prioridade e baixo 3 entre 0 e 9, sendo 9 a maior prioridade
        // e o tempo de vida e de 5000ms ou 5s
        Message message = session.createTextMessage("INFO | Apache ActiveMQ Custom Test Log !");

        producer.send(message, DeliveryMode.NON_PERSISTENT, 3, 5000);

        session.close();
        connection.close();
        context.close();
    }

}
