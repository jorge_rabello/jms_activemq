package br.com.jorgerabellodev.jms.aplicacao.logs;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

public class TesteConsumidor {

    public static void main(String[] args) throws Exception {

        // estabelecendo uma conexão
        InitialContext context = new InitialContext();

        ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("ConnectionFactory");
        factory.setTrustAllPackages(true);

        Connection connection = factory.createConnection();
        connection.start();

        // cria um consumer
        Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
        Destination fila = (Destination) context.lookup("LOG");
        MessageConsumer consumer = session.createConsumer(fila);

        // recebe a mensagem
        consumer.setMessageListener(message -> {
            TextMessage textMessage = (TextMessage) message;
            try {
                session.commit();
                System.out.println(textMessage.getText());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });

        new Scanner(System.in).nextLine();

        session.close();
        connection.close();
        context.close();
    }

}
