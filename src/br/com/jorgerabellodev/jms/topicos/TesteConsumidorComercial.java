package br.com.jorgerabellodev.jms.topicos;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;

import br.com.jorgerabellodev.jms.modelo.Pedido;

public class TesteConsumidorComercial {
    public static void main(String[] args) throws Exception {

        // estabelecendo uma conexão
        InitialContext context = new InitialContext();
        ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("ConnectionFactory");
        factory.setTrustAllPackages(true);

        Connection connection = factory.createConnection();

        connection.setClientID("comercial");
        connection.start();

        // cria um consumer
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topico = (Topic) context.lookup("loja");
        MessageConsumer consumer = session.createDurableSubscriber(topico, "assinatura");

        // recebe a mensagem
        consumer.setMessageListener(message -> {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                Pedido pedido = (Pedido) objectMessage.getObject();
                System.out.println(pedido.getCodigo());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });

        new Scanner(System.in).nextLine();

        session.close();
        connection.close();
        context.close();
    }

}
