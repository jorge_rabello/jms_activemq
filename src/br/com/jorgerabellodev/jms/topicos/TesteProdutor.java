package br.com.jorgerabellodev.jms.topicos;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

import br.com.jorgerabellodev.jms.modelo.Pedido;
import br.com.jorgerabellodev.jms.modelo.PedidoFactory;

public class TesteProdutor {
    public static void main(String[] args) throws Exception {

        // estabelecendo uma conexão
        InitialContext context = new InitialContext();
        ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("ConnectionFactory");
        factory.setTrustAllPackages(true);
        Connection connection = factory.createConnection();
        connection.start();

        // cria um consumer
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination topico = (Destination) context.lookup("loja");

        MessageProducer producer = session.createProducer(topico);

        Pedido pedido = new PedidoFactory().geraPedidoComValores();

        Message message = session.createObjectMessage(pedido);
        message.setBooleanProperty("ebook", false);

        producer.send(message);

        session.close();
        connection.close();
        context.close();
    }
}
